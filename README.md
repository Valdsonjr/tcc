
# Trabalho de Conclusão de Curso

Instalação e Uso:

    1. Copiar a pasta Motor para o diretório libraries do seu sketchbook do arduino.
    2. Compilar/Carregar o arquivo SliderCam.ino para o arduino 
        (OBS: eu uso o Arduino Uno, não sei se funciona em outras placas)
    3. Rodar o código python. (É necessário ter as bibliotecas opencv-python, imutils, pyserial e numpy instaladas) 
        (Lembre de checar se o arduino está conectado na porta usada dentro do código python.)
