from __future__ import print_function
import cv2
import keras
import numpy as np
import os
import random
from keras.layers import Dense, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.models import Sequential


# carrega o modelo
model = keras.models.load_model("minhaNet.hdf5")

# carrega o mosaico
TEMP = "/Users/ValdsonJr/Desktop/output.jpg"

def picotador(path, stride=32, largura=120):

    img = cv2.imread(path, 0)
    altura, comprimento = np.shape(img)
    altura = 120
    img = cv2.resize(img, (comprimento, altura))

    i = 0
    retorno = []

    while (i * stride + largura <= comprimento):
        newimg = img[0:altura, i*stride: i * stride + largura]
        newimg = (newimg.astype('float32') - 255 / 2) / 255
        retorno.append(newimg)
        i = i + 1

    return retorno

# exemplo de vetor de resultados esperados do mosaico
res = [[0,0,0], [0,0,0], [0,0,0], [0,0,0], [0,0,0],
       [0,0,0], [0,0,0], [0,0,0], [0,0,0], [0,0,0],
       [0,0,0], [0,0,0], [0,0,0], [0,0,0], [0,0,0],
       [0,0,0], [0,0,0], [0,0,1], [0,0,1], [0,0,1]]

lista = picotador(TEMP)
lista = np.array(lista)
lista = lista.reshape(lista.shape[0], 120, 120, 1)
lista = model.evaluate(lista, res)
print(model.metrics_names)
print(lista)