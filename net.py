from __future__ import print_function
import cv2
import keras
import numpy as np
import os
import random
from keras.layers import Dense, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.models import Sequential
import matplotlib.pylab as plt


def formataEntrada(dataset):
    return (dataset.reshape(dataset.shape[0],
                            img_x,
                            img_y,
                            1).astype('float32') - 255 / 2) / 255

def lerNum(folder_list, num):
    imagens = []
    for folder in folder_list:
        files = os.listdir(folder)
        random.shuffle(files)
        files = files[:num]
        for file in files:
            ff = cv2.imread(os.path.join(folder, file), 0)
            ff = ff[60:180][60:180]
            imagens.append(ff)

    return imagens

def replicate(num):
    def rep(lista):
        res = []
        for i in range(num):
            res.append(lista)
        return res
    return rep


def flatten(listas):
    res = []
    for sublista in listas:
        for item in sublista:
            res.append(item)


batch_size = 32
epochs = 10
img_x = 120
img_y = 120
sample_size = 250
input_shape = (img_x, img_y, 1)

MYPATH = "/Users/ValdsonJr/Desktop/Val/Code/Python/serial/"
FOO = ["bananada", "leitemoca", "milho"]
num_classes = len(FOO)

outputs = [[1, 0, 0],
           [0, 1, 0],
           [0, 0, 1]]

train_folders = list(map(lambda x: MYPATH + x, FOO))

print(train_folders)

x_train = formataEntrada(np.array(lerNum(train_folders, sample_size)))

model = Sequential()
model.add(Conv2D(32, kernel_size=(5, 5), strides=(1, 1),
                 activation='relu',
                 input_shape=input_shape))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
model.add(Conv2D(32, kernel_size=(5, 5), strides=(1, 1),
                 activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
model.add(Conv2D(64, kernel_size=(5, 5), strides=(1, 1),
                 activation='relu'))
model.add(Conv2D(64, (5, 5), activation='relu'))
model.add(Conv2D(64, kernel_size=(5, 5), strides=(1, 1),
                 activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Flatten())
model.add(Dense(1000, activation='relu'))
model.add(Dense(500, activation='relu'))
model.add(Dense(num_classes, activation='sigmoid'))

MODELFILE = "minhaNet.hdf5"

model.compile(loss='binary_crossentropy',
            optimizer='adam',
            metrics=['accuracy'])

labels = np.array(list(map(replicate(sample_size), outputs)))
labels = labels.reshape(labels.shape[0]*labels.shape[1], labels.shape[2])

checkpoint = keras.callbacks.ModelCheckpoint(MODELFILE,
                                            monitor='val_acc',
                                            verbose=1,
                                            save_best_only=True,
                                            mode='max')

history = model.fit(x_train, labels,
                    batch_size=batch_size,
                    epochs=epochs,
                    verbose=1,
                    validation_split=0.2,
                    callbacks=[checkpoint])

print(history.history.keys())
#  "Accuracy"
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.show()
# "Loss"
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.show()
