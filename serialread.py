""" Module docstring """
import serial
import numpy as np
import cv2
import imutils

CAM = "/dev/cu.usbmodemFA131"
MOT = "/dev/cu.wchusbserialfd110"


def sync(ser, command):
    """ Awaits for the <command> """

    index = 0
    while index < len(command):
        if ser.read() == str.encode(command[index]):
            index += 1
        else:
            index = 0


def read(port1, port2, height, width):
    """ Reads images of size
    <height*width> from serial <port> """

    cam = serial.Serial(port=port1,
                        baudrate=1000000,
                        bytesize=serial.EIGHTBITS,
                        parity=serial.PARITY_NONE,
                        stopbits=serial.STOPBITS_ONE,
                        timeout=20)
    mot = serial.Serial(port=port2, baudrate=9600)

    print("Connected to port1: " + port1 + " for CAM control.")
    print("Connected to port2: " + port2 + " for MOT control.")

    for counter in range(1, 30):

        print("Searching for image ...")
        sync(cam, "*RDY*")
        print("Image found: " + str(counter))

        rgb = cam.read(height * width)
        img = np.fromstring(rgb, np.uint8).reshape((height, width))

        # rotaciona a imagem
        img2 = imutils.rotate_bound(img, 180)
        img3 = cv2.equalizeHist(img2)

        cv2.imshow("cameraman", img3)
        cv2.imwrite("out/" + str(counter) + ".jpg", img3)

        if counter % 10 == 0:
            mot.write(b's')
        else:
            mot.write(b'a')

    print("Finishing...")
    cam.close()
    mot.close()

read(CAM, MOT, 240, 320)
