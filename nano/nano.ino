/* 
 *  Universidade Federal de Sergipe
 *  Centro de Ciências Exatas e Tecnologia  
 *  Departamento de Computação
 *  
 *  Ciência da Computação
 *  Valdson Francisco Silva Santos Júnior
 *  
 *  07/2017
 *  
 *  Programa a ser embarcado no Arduino Nano para controlar
 *  o motor do carro que se move no trilho.
 */

const int dir_pin = 5; // Associa o controle de direção ao pino 5;
const int spd_pin = 6; // Associa a velocidade (PWM) ao pino 6;
const int global_speed = 245;
const int global_duration = 80;
const int global_duration_return = global_duration * 7;

// Declaracao das variaveis:
char comando;

void parar(){
  digitalWrite(dir_pin, LOW);
  analogWrite(spd_pin, LOW);
}

void clockwise(int spd, int dur){
  digitalWrite(dir_pin, HIGH);
  analogWrite(spd_pin, 255-spd);
  delay(dur);
  parar();
}

void counter_clockwise(int spd, int dur){
  digitalWrite(dir_pin, LOW);
  analogWrite(spd_pin, spd);
  delay(dur);
  parar();
}
void setup() {
  // Configura os pinos como pinos de saida.
  pinMode(dir_pin, OUTPUT);
  pinMode(spd_pin, OUTPUT);
 
  // Inicializa a porta serial.
  Serial.begin(9600); 
  while (!Serial) {
  }
}

void loop() {
  if (Serial.available() > 0) {
    comando = Serial.read();
    if (comando == 'a') { // avança com o carro 1 unidade para a esquerda.
      clockwise(global_speed, global_duration);
    } else if (comando == 's') { // retorna o carro para o início do trilho.
      counter_clockwise(global_speed, global_duration_return);  
    } 
  }
}
