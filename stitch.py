# USO
# python stitch.py -f 11 -l 20 -o output

from pyimagesearch.panorama import Stitcher
import numpy as np
import argparse
import imutils
import cv2
import functools

MYPATH = "/Users/ValdsonJr/Desktop/tcc/slidercam_tcc/out1/"

def autocrop(image, threshold=0):
    """Crops any edges below or equal to threshold
        Crops blank image to 1x1.
        Returns cropped image.
        """
    if len(image.shape) == 3:
        flatImage = np.max(image, 2)
    else:
        flatImage = image
    assert len(flatImage.shape) == 2
    
    rows = np.where(np.max(flatImage, 0) > threshold)[0]
    if rows.size:
        cols = np.where(np.max(flatImage, 1) > threshold)[0]
        image = image[cols[0]: cols[-1] + 1, rows[0]: rows[-1] + 1]
    else:
        image = image[:1, :1]
    
    return image

stitcher = Stitcher()

def auxiliary(output, image):
    img = cv2.imread(MYPATH + str(image) + ".jpg")
    (res, matches) = stitcher.stitch([img, output], showMatches=True)
    cv2.imwrite(args["output"]+ "matches" + ".jpg", matches)
    
    return autocrop(res)

ap = argparse.ArgumentParser()
ap.add_argument("-f", "--first", required=True, help="Number of the first image")
ap.add_argument("-l", "--last", required=True, help="Number of the last image")
ap.add_argument("-o", "--output", required=True, help="Path to the output image")
args = vars(ap.parse_args())

img = functools.reduce(auxiliary, range(int(args["last"]) - 1, int(args["first"]) - 1, - 1), cv2.imread(MYPATH + args["last"] + ".jpg"))
#img = functools.reduce(auxiliary, range(int(args["first"]) + 1, int(args["last"]) + 1, + 1), cv2.imread(MYPATH + args["first"] + ".jpg"))

cv2.imwrite(args["output"] + ".jpg", img)
