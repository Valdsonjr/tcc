import numpy as np
import cv2

TEMP = "/Users/ValdsonJr/Desktop/output.jpg"


def picotador(path, stride=32, largura=240):

    img = cv2.imread(path, 0)
    altura, comprimento = np.shape(img)

    i = 0
    retorno = []

    while (i * stride + largura <= comprimento):
        newimg = img[0:altura, i*stride: i * stride + largura]
        retorno.append(newimg)
        i = i + 1

    return retorno

LISTA = picotador(TEMP)

for img in LISTA:
    cv2.imshow("img", img)
    cv2.waitKey(0)
